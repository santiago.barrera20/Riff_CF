# Riff_CF

##PASO PARA DEPLOY DE LAS CLOUD FUNCTIONS

**1.** Descargar el código fuente de  https://gitlab.com/santiago.barrera20/Riff_CF 

**2.** Ejecutar el siguiente comando para ejecutar en localhost
        ***firebase serve --only functions***

**Nota:**
Para desplegar en producción debe ejecutar el siguiente código
***firebase deploy --only functions:v1***

**3.** Para ver logs en gcloud usar:
***gcloud functions logs read***



