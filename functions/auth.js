const express = require('express')
const router = express.Router()
module.exports = router

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
const admin = require('firebase-admin')

let db = admin.firestore()

//----------------- CF GET AUTH TOKEN-----------------
router.get('/CF_get_auth_token', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Basic ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const base64Credentials = req.headers.authorization.split(' ')[1]
    const credentials = Buffer.from(base64Credentials, 'base64').toString(
      'ascii'
    )
    const [username, password] = credentials.split(':')

    if (
      username !== req.app.locals.global_vars.APIUser ||
      password !== req.app.locals.global_vars.APIPassword
    ) {
      return res.status(401).json({
        code: 401,
        message: 'Invalid Authentication Credentials'
      })
    } else {
      // All Code
      let ip_address = req.headers.ip_address
      let tokensRef = db.collection('tokens')
      try {
        // IF NEED TO BE CREATED A NEW TOKEN
        let tokenData = {
          remote_address: ip_address
        }
        let newtoken = jwt.sign(
          tokenData,
          req.app.locals.global_vars.APIPassword,
          {
            expiresIn: 60 * 60 * 12 // expires in 12 hours
          }
        )

        // DECODE TOKEN AND SAVE IT IN THE DB
        let decodedtoken = jwt.verify(
          newtoken,
          req.app.locals.global_vars.APIPassword,
          (err, decoded) => {
            if (decoded) {
              decoded.iat = new Date(decoded.iat * 1000)
              decoded.exp = new Date(decoded.exp * 1000)
              decoded.token = newtoken
              tokensRef.add(decoded)

              res.send({
                code: 1,
                token: newtoken
              })
            } else {
              console.log(err)

              res.send({
                code: 0,
                description:"Error decoding token"
              })
            }

          }
        )


        // queryRef = tokensRef
        //   .where('remote_address', '==', ip_address)
        //   .where('exp', '>=', new Date())
        // queryRef.get().then(querySnapshot => {
        //   if (!querySnapshot.empty) {
        //     // IF FIND A VALID TOKEN
        //     let token = querySnapshot.docs[0].data().token
        //     res.send({
        //       code: 1,
        //       token: token
        //     })
        //   } else {
        //
        //   }
        // })
      } catch (error) {
        res.send({
          code: 2,
          message: 'Error: ' + error
        })
      }
    }
  }
  return null
})
