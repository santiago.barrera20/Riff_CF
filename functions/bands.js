const express = require("express");
const router = express.Router();
module.exports = router;

const functions = require("firebase-functions");
var jwt = require("jsonwebtoken");
const admin = require("firebase-admin");

let db = admin.firestore();

//-----------------CF GET AVAILABLE BANDS-----------------
router.get("/CF_get_available_bands", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        decoded
        // IF VALID RETURN BANDS
        let bands = [];
        db.collection("bands")
          .get()
          .then((snapshot) => {
            snapshot.docs.forEach((snap) => {
              bands.push(snap.data());
            });
            res.send({
              code: 1,
              message: "Bands returned successfully",
              data: bands,
            });
            return null
          });
      }
    });
  }
  return null
});

//-----------------CF CREATE NEW BAND-----------------
router.post("/CF_create_band", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF BAND ALREADY EXISTS
        if (req.body.name) {
          let dataBand = req.body;
          dataBand.band_id = dataBand.name.replace(/ /g, "_").toLowerCase();
          let bandRef = db.collection("bands").doc(dataBand.band_id);
          let getDoc = bandRef.get().then((doc) => {
            if (!doc.exists) {
              // CREATE THE NEW BAND
              bandRef.set(dataBand);
              res.send({
                code: 1,
                message: "Band created successfully",
                band_id: dataBand.band_id
              });
            } else {
              // BAND ALREADY EXISTS
              res.send({
                code: 2,
                message: "Band already exists, try with another name",
              });
            }
          });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
  return null
});

//-----------------CF READ BAND-----------------
router.post("/CF_read_band", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID RETURN BAND
        if (req.body.band_id) {
          // IF EXISTS THE REQUESTED BAND_ID
          let userRef = db.collection("bands").doc(req.body.band_id);
          userRef.get().then((doc) => {
            if (doc.exists) {
              // RETURN THE BAND
              res.send({
                code: 1,
                message: "Data returned succesfully",
                data: doc.data(),
              });
            } else {
              // BAND EXISTS
              res.send({
                code: 2,
                message: "Band not found",
              });
            }
          });
        } else {
          // REQUESTED BAND UNDEFINED
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
  return null
});

//-----------------CF UPDATE BAND-----------------
router.put("/CF_update_band", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID VALIDATE IF BAND EXISTS
        if (req.body.band_id) {
          let bandRef = db.collection("bands").doc(req.body.band_id);
          bandRef.get().then((doc) => {
            if (doc.exists) {
              // UPDATE BAND
              let dataBand = req.body;
              delete dataBand.band_id;
              bandRef.update(dataBand);
              res.send({
                code: 1,
                message: "Band updated successfully",
              });
            } else {
              // BAND DOESN'T EXISTS
              res.send({
                code: 2,
                message: "Band does not exist, try with another band_id",
              });
            }
          });
        } else {
          res.send({
            code: 3,
            message: "Invalid band_id",
          });
        }
      }
    });
  }
  return null
});

//-----------------CF DELETE USER-----------------
router.delete("/CF_delete_band", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID VALIDATE IF BAND EXISTS
        if (req.body.band_id) {
          let bandRef = db.collection("bands").doc(req.body.band_id);
          bandRef.get().then((doc) => {
            if (doc.exists) {
              // DELETE BAND
              bandRef.delete();

              res.send({
                code: 1,
                message: "Band deleted successfully",
              });
            } else {
              // BAND DOESN'T EXISTS
              res.send({
                code: 2,
                message: "Band does not exist, try with another band_id",
              });
            }
          });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
  return null
});
