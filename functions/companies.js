const express = require('express')
const router = express.Router()
module.exports = router

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
const admin = require('firebase-admin')

let db = admin.firestore()

//-----------------CF GET AVAILABLE COMPANIES-----------------
router.get('/CF_read_available_companies', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(200)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(200).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        let companies = []
        db.collection('companies').where("active", "==", 1)
          .get()
          .then(snapshot => {
            snapshot.docs.forEach(snap => {
              let new_company = snap.data()
              delete new_company.services
              delete new_company.admin_user
              delete new_company.list_images
              delete new_company.description
              delete new_company.active
              delete new_company.schedule
              delete new_company.creation_date
              companies.push(new_company)

            })
            res.send({
              code: 1,
              message: 'Companies returned successfully',
              data: companies
            })
          })
      }
    })
  }
})

//-----------------CF CREATE NEW COMPANY-----------------
router.post('/CF_create_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataCompany = req.body
        if (
          dataCompany.admin_user &&
          dataCompany.name &&
          dataCompany.active &&
          dataCompany.address &&
          dataCompany.phone &&
          dataCompany.coordinates &&
          dataCompany.icon_image &&
          dataCompany.description &&
          dataCompany.list_images &&
          dataCompany.services
        ) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '==', req.app.locals.global_vars.superAdminRole)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN ADD THE NEW  COMPANY
                dataCompany.company_id = dataCompany.name
                  .replace(/ /g, '_')
                  .toLowerCase()
                dataCompany.creation_date = new Date()
                let companyRef = db
                  .collection('companies')
                  .doc(dataCompany.company_id)
                let getDoc = companyRef
                  .get()
                  .then(doc => {
                    if (!doc.exists) {
                      // CREATE THE NEW COMPANY
                      companyRef.set(dataCompany)
                      res.send({
                        code: 1,
                        message: 'Company created successfully',
                        company_id: dataCompany.company_id
                      })
                    } else {
                      // COMPANY ALREADY EXISTS
                      res.send({
                        code: 4,
                        message: 'Company already exists, try with another name'
                      })
                    }
                  })
                  .catch({})
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid superadministrator'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF READ COMPANY-----------------
router.post('/CF_read_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataCompany = req.body
        if (dataCompany.company_id) {
          // PROMISE THAT RETURNS A COMPANY IF EXISTS
          let promise = new Promise(((resolve, reject) => {
            let companyRef = db
              .collection('companies')
              .doc(dataCompany.company_id)
            let getDoc = companyRef.get().then(doc => {
              if (doc.exists) {
                let company = doc.data()
                delete company.creation_date
                delete company.admin_user
                delete company.active
                resolve(company)
              } else {
                reject()
              }
            })

          }));


          // PROMISE THAT RETURNS THE COMPANY SERVICES
          function getServicesDetails(company) {
            return new Promise(((resolve, reject) => {
              let listServices = []
              let services = []
              db.collection("services").where("active", "==", 1)
                .get()
                .then((snapshot) => {
                  snapshot.docs.forEach((snap) => {
                    services.push(snap.data());
                  });

                  for (let i = 0; i < company.services.length; i++) {
                    for (let j = 0; j < services.length; j++) {
                      if (company.services[i] === services[j].service_id) {
                        let newService = services[j]
                        delete newService.admin_user
                        delete newService.active
                        listServices.push(newService)
                        break;
                      }

                    }
                  }
                  company.services = listServices
                  resolve(company)
                });

            }));
          }

          //FUNCTION THAT RETURN COMPANY ROOMS
          function getRooms(company) {
            return new Promise(((resolve, reject) => {
              let rooms = [{"name":"sala1"}]
              company.rooms = []
              db.collection("rooms").where("company_id", "==", company.company_id).where("active", "==", 1)
                .get()
                .then((snapshot) => {
                  // console.log(snapshot.docs)
                  snapshot.docs.forEach((snap) => {
                    company.rooms.push(
                      snap.data()
                      // "name":snap.get("name"),
                      // "room_id":snap.get("room_id"),
                      // "description":snap.get("description"),
                      // "icon_image":snap.get("icon_image")
                    );
                  });
                  // company.rooms = rooms
                  resolve(company);
                })

            }));
          }



          promise.then((result) => {
            getServicesDetails(result).then(r => {
              getRooms(r).then(ro => {
                // RETURN COMPANY
                ro.rooms.forEach(room => {
                  delete room.list_images
                  delete room.prices
                  delete room.active
                  delete room.admin_user
                  delete room.creation_date
                  delete room.services
                });
                res.send({
                  code: 1,
                  message: 'Data returned successfully',
                  data: ro
                })
              })

            }).catch(() => {
              console.log('Algo salió mal');
            });
          }, () => {
            // COMPANY ALREADY EXISTS
            res.send({
              code: 4,
              message: 'Company does not exist, try with another name'
            })
          })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF UPDATE COMPANY-----------------
router.put('/CF_update_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataCompany = req.body
        if (dataCompany.admin_user && dataCompany.company_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '==', req.app.locals.global_vars.superAdminRole)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN RETURN COMPANY
                let companyRef = db
                  .collection('companies')
                  .doc(dataCompany.company_id)
                let getDoc = companyRef.get().then(doc => {
                  if (doc.exists) {
                    // UPDATE COMPANY
                    delete dataCompany.admin_user
                    delete dataCompany.company_id
                    companyRef.update(dataCompany)
                    res.send({
                      code: 1,
                      message: 'Company updated successfully'
                    })
                  } else {
                    // COMPANY ALREADY EXISTS
                    res.send({
                      code: 4,
                      message: 'Company does not exist, try with another name'
                    })
                  }
                })
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid superadministrator'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF DELETE COMPANY-----------------
router.delete('/CF_delete_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataCompany = req.body
        if (dataCompany.admin_user && dataCompany.company_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '==', req.app.locals.global_vars.superAdminRole)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN DELETE COMPANY
                let companyRef = db
                  .collection('companies')
                  .doc(dataCompany.company_id)
                let getDoc = companyRef.get().then(doc => {
                  if (doc.exists) {
                    // DELETE COMPANY
                    companyRef.delete()
                    res.send({
                      code: 1,
                      message: 'Company deleted successfully'
                    })
                  } else {
                    // COMPANY ALREADY EXISTS
                    res.send({
                      code: 4,
                      message: 'Company does not exist, try with another name'
                    })
                  }
                })
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid superadministrator'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})
