'use strict'

const functions = require('firebase-functions')
const admin = require('firebase-admin')
var jwt = require('jsonwebtoken')
// var timeout = require('connect-timeout');
// Config in NodeJS local server

// let serviceAccount = require('../Cloud_functions/functions/riff-355e5-3035a19db813.json')
// let serviceAccount = require('./riff-355e5-3035a19db813.json')

// Initialize App in local server
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   storageBucket: "gs://riff-355e5.appspot.com"
// })

admin.initializeApp(functions.config().firebase)

const express = require('express')
const cors = require('cors')({ origin: true })

// Import Sentry
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");

const app = express()

// ###### SENTRY CONFIG ######
Sentry.init({
  dsn: "https://f35ff09222804c709571b4a1af8c6453@o471465.ingest.sentry.io/5503575",
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Tracing.Integrations.Express({ app }),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(
  Sentry.Handlers.errorHandler({
    shouldHandleError(error) {
      // Capture all 404 and 500 errors
      if (error.status === 404 || error.status === 400 || error.status === 500)  {
        return true;
      }
      return false;
    },
  })
);

app.use(cors)
// app.use(timeout(20000));
app.use((req, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*')

  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  )

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', false)

  // Pass to next layer of middleware
  next()
})

// Set global vars
app.locals.global_vars = {
  APIUser: 'adminriff',
  APIPassword: 'riff2020',
  superAdminRole: 10,
  companyAdmin: 4
}


// Import group functions from other files
const auth_route = require('./auth');
app.use('/auth', auth_route);

const bands_route = require('./bands');
app.use('/bands', bands_route);

const users_route = require('./users');
app.use('/users', users_route);

const companies_route = require('./companies');
app.use('/companies', companies_route);

const services_route = require('./services');
app.use('/services', services_route);

const roles_route = require('./roles');
app.use('/roles', roles_route);

const rooms_route = require('./rooms');
app.use('/rooms', rooms_route);

const reservations_route = require('./reservations');
app.use('/reservations', reservations_route);

// This HTTPS endpoint can only be accessed by your Firebase Users.
// Requests need to be authorized by providing an `Authorization` HTTP header
// with value `Bearer <Firebase ID Token>`.
exports.staging = functions.https.onRequest(app)
exports.v1 = functions.https.onRequest(app)
console.log(`
______    ___   _______  _______    _______  _______  _______  ___   _  _______  __    _  ______  
|    _ |  |   | |       ||       |  |  _    ||   _   ||       ||   | | ||       ||  |  | ||      | 
|   | ||  |   | |    ___||    ___|  | |_|   ||  |_|  ||       ||   |_| ||    ___||   |_| ||  _    |
|   |_||_ |   | |   |___ |   |___   |       ||       ||       ||      _||   |___ |       || | |   |
|    __  ||   | |    ___||    ___|  |  _   | |       ||      _||     |_ |    ___||  _    || |_|   |
|   |  | ||   | |   |    |   |      | |_|   ||   _   ||     |_ |    _  ||   |___ | | |   ||       |
|___|  |_||___| |___|    |___|      |_______||__| |__||_______||___| |_||_______||_|  |__||______|                                                                      
                                                                               
`);