// import firebase from 'firebase';
// import 'firebase/storage';

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin')

// Config in NodeJS local server
// let serviceAccount = require('C:/Users/developer/Documents/4-personal/reservas_riff/Version_2/Cloud_functions/functions/riff-355e5-3035a19db813.json')

// Initialize App in local server
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount)
// })

// Initialize App
admin.initializeApp(functions.config().firebase)

const storage = admin.storage()

// Import group functions from other files
exports.bands = require('./bands')
exports.auth = require('./auth')
exports.users = require('./users')

let db = admin.firestore()

//----------------- CF GET AUTH TOKEN-----------------
exports.CF_test = functions.https.onRequest((req, res) => {
  res.status(200).json({ code: 1, message: 'Cloud function works!' })
})


