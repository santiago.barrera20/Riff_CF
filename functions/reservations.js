const express = require('express')
const router = express.Router()
var moment = require('moment-timezone');
moment().format();
module.exports = router

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
const admin = require('firebase-admin')
let db = admin.firestore()


//-----------------CF CREATE NEW RESERVATION-----------------
router.post('/:company_id/:room_id/CF_create_reservation', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let company_id = req.params.company_id
        let room_id = req.params.room_id
        let dataRes = req.body
        if (
          dataRes.date &&
          dataRes.user_id &&
          dataRes.time_range
        ) {
          dataRes.company_id = company_id
          dataRes.room_id = room_id
          dataRes.total_price = 0
          dataRes.created_at = moment().tz('America/Bogota').format()

          let splitdate = dataRes.date.split("-")
          dataRes.initial_date = moment([splitdate[0], parseInt(splitdate[1]) - 1, splitdate[2], parseInt(dataRes.time_range[0].time.split("-"[0]))+5]).utcOffset(-5).format()
          // console.log(moment().utcOffset(-5).format())
          // console.log(moment([splitdate[0],parseInt(splitdate[1])-1,splitdate[2],dataRes.time_range[0].time.split("-"[0])]).utcOffset(-5).format())
          // moment([moment().year(),moment().month(),moment().date()]).utcOffset(-5).format()
          console.log('reservation initial date: '+dataRes.initial_date);
          if (moment().utcOffset(-5).format() <= dataRes.initial_date) {
            dataRes.time_range.forEach(el => {
              dataRes.total_price += el.price
            });
            let reservationRef = db.collection('reservations')
            reservationRef.where("company_id", "==", company_id).where("room_id", "==", room_id).where("date", "==", dataRes.date)
              .get().then(snapshot => {
                if (snapshot.docs.length > 0) {
                  // console.log("si existen reservas")
                  let invalidReservation = false
                  let reservations = []//= reservationRef.get("time_range")

                  snapshot.docs.forEach(el => {
                    el.data().time_range.forEach(element => {
                      reservations.push(element)
                    });
                  })

                  for (let i = 0; i < dataRes.time_range.length; i++) {
                    for (let j = 0; j < reservations.length; j++) {
                      if (dataRes.time_range[i].time === reservations[j].time) {
                        invalidReservation = true
                        break
                      }
                    }
                  }
                  if (!invalidReservation) {

                    // crea id de acuerdo al max_id generado
                    reservationRef.get().then(function (documentSnapshots) {
                      // Get the last visible document
                      let reservation_id = documentSnapshots.docs.length + 1
                      dataRes.reservation_id = reservation_id
                      dataRes.status = 'active'
                      let document_id = reservationRef.doc().id
                      reservationRef.doc(document_id).set(dataRes)
                      res.send({
                        code: 1,
                        message: 'Reservation created successfully',
                        created_at: dataRes.created_at,
                        reservation_id: document_id
                      })
                    })

                  } else {
                    res.send({
                      code: 0,
                      message: 'The time range chosen is already busy, try with another time range'
                    })
                  }
                } else {

                  // crea id de acuerdo al max_id generado
                  reservationRef.get().then(function (documentSnapshots) {
                    // Get the last visible document
                    let reservation_id = documentSnapshots.docs.length + 1
                    dataRes.reservation_id = reservation_id
                    dataRes.status = 'active'
                    let document_id = reservationRef.doc().id
                    reservationRef.doc(document_id).set(dataRes)
                    res.send({
                      code: 1,
                      message: 'Reservation created successfully',
                      created_at: dataRes.created_at,
                      reservation_id: document_id
                    })
                  })
                }
              })

          } else {
            res.send({
              code: 0,
              message:
                'Invalid date',
                date_given: dataRes.initial_date
            })
          }

        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

router.post("/CF_reservations_list", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(
      bearerToken,
      req.app.locals.global_vars.APIPassword,
      (err, decoded) => {
        if (err) {
          res.status(401).send({
            code: 401,
            message: "Invalid or expired token, please generate a new one",
          });
        } else {
          let data_body = req.body;
          if (
            data_body.data_per_page &&
            data_body.start_after >=0
            ) {
              const dataPerPage = data_body.data_per_page;
              const startAfter = data_body.start_after;
              const reservationsCollection = db.collection("reservations")
              // si existe el status filtre por status, de lo contrario traiga todo el listado              
              if(!data_body.status && !data_body.user_id) {
                reservationsCollection.orderBy("reservation_id")
                .startAfter(startAfter)
                .limit(dataPerPage)
                .get()
                .then( async (snapshot) => {
                  const response = await resultActions(snapshot)
                  res.send({
                    code: 1,
                    message: `Reservations returned successfully`,
                    data: response
                  });
                })
                .catch((error) => {
                  console.error(error);
                });
              }
              if(data_body.status && !data_body.user_id) {
                reservationsCollection.where("status", "==" , data_body.status)
                .orderBy("reservation_id")
                .startAfter(startAfter)
                .limit(dataPerPage)
                .get()
                .then(async(snapshot) => {
                  const response = await resultActions(snapshot)
                  res.send({
                    code: 1,
                    message: `Reservations returned successfully`,
                    data: response
                  });
                })
                .catch((error) => {
                  console.error(error);
                });
              }
              if(!data_body.status && data_body.user_id) {
                reservationsCollection.where("user_id", "==" , data_body.user_id)
                .orderBy("reservation_id")
                .startAfter(startAfter)
                .limit(dataPerPage)
                .get()
                .then(async (snapshot) => {
                  const response = await resultActions(snapshot)
                  res.send({
                    code: 1,
                    message: `Reservations returned successfully`,
                    data: response
                  });
                })
                .catch((error) => {
                  console.error(error);
                });
              }
              if(data_body.status && data_body.user_id) {
                reservationsCollection.where("user_id", "==" , data_body.user_id)
                .where("status", "==" , data_body.status)
                .orderBy("reservation_id")
                .startAfter(startAfter)
                .limit(dataPerPage)
                .get().then( async(snapshot) => {
                  const response = await resultActions(snapshot)
                  res.send({
                    code: 1,
                    message: `Reservations returned successfully`,
                    data: response
                  });
                })
                .catch((error) => {
                  console.error(error);
                });
              }

            
          } else {
            res.send({
              code: 3,
              message:
                "Incomplete or invalid parameters, please review the documentation first",
            });
          }
        }
      }
    );
  }
});

const getArrayCompaniesRooms = (array) => {
  let arrayFilter = array.map((elem) => elem.company_id);
  let arrayResponse = arrayFilter.filter(function (item, pos) {
    return arrayFilter.indexOf(item) == pos;
  });
  return arrayResponse;
};

const getAllCompanies = (arrayListCompanies) => {
  return db
    .collection("companies")
    .where("company_id", "in", arrayListCompanies)
    .get()
    .then((snapshot) => {
      let companies = [];
      snapshot.docs.forEach((element) => {
        let company = {
          name: element.get("name"),
          company_id: element.id,
          icon_image: element.get("icon_image")
        };
        companies.push(company);
      });
      return companies;
    })
    .catch((error) => {
      console.error(error);
    });
};

const getAllRooms = (arrayListCompaniesId, arrayCompanies) => {
  return db
    .collection("rooms")
    .where("company_id", "in", arrayListCompaniesId)
    .get()
    .then((snapshot) => {
      let rooms = [];
      snapshot.docs.forEach((element) => {
        let room = {
          company: arrayCompanies.find(
            (elem) => elem.company_id === element.get("company_id")
          ),
          room: {
            room_id: element.get("room_id"),
            name: element.get("name"),
          },
        };
        rooms.push(room);
      });
      return rooms;
    })
    .catch((error) => {
      console.error(error);
    });
};

const resultActions = async (snapshot, res) => {
  let reservationsArray = [];
  snapshot.docs.forEach((element) => {
    let reservation = element.data();
    reservation.id_qr = element.id;
    delete reservation.initial_date;
    reservationsArray.push(reservation);
  });
  // if there are reservations
    if (reservationsArray.length > 0) {
      let arrayListCompanies = getArrayCompaniesRooms(
        reservationsArray,
        "company_id"
      );
      const companies = await getAllCompanies(arrayListCompanies);
      const roomsCompanies = await getAllRooms(
        companies.map((elem) => elem.company_id),
        companies
      );
      reservationsArray = reservationsArray.map((elem) => {
        let response = {
          reservation_id: elem.reservation_id,
          reservation_id_qr: elem.id_qr,
          band: elem.band,
          user_id: elem.user_id,
          status: elem.status,
          total_price: elem.total_price,
          company_id: elem.company_id,
          company_name: companies.find(
            (elemFind) =>
              elemFind.company_id === elem.company_id
          ).name,
          icon_image: roomsCompanies.find(
            (elemFind) =>
              elemFind.company.company_id === elem.company_id
          ).company.icon_image,
          date: elem.date,
          room_id: elem.room_id,
          room_name: roomsCompanies.find(
            (elemFind) => elemFind.room.room_id === elem.room_id
          ).room.name,
          time_range: elem.time_range
        };
        return response;
      });
    }
    return reservationsArray

}

