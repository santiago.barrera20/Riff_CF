const express = require("express");
const router = express.Router();
module.exports = router;

const functions = require("firebase-functions");
var jwt = require("jsonwebtoken");
const admin = require("firebase-admin");

let db = admin.firestore();

//-----------------CF GET AVAILABLE ROLES-----------------
router.post("/CF_read_available_roles", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF SUPERADMIN USER SENT
        if (req.body.admin_user) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN RETURN ALL ROLES
                let roles = [];
                db.collection("roles")
                  .get()
                  .then((snapshot) => {
                    snapshot.docs.forEach((snap) => {
                      roles.push(snap.data());
                    });
                    res.send({
                      code: 1,
                      message: "Roles returned successfully",
                      data: roles,
                    });
                  });
              } else {
                res.send({
                  code: 2,
                  message: "Unauthorized admin_user",
                });
              }
            })
            .catch((err) => {
              console.log("Error getting document", err);
            });
        } else {
          // IF NOT ADMIN USER SENT
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF CREATE NEW ROLE-----------------
router.post("/CF_create_role", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF ROLE ALREADY EXISTS
        let dataRole = req.body;
        try {
          if (dataRole.admin_user && Number.isInteger(dataRole.role_id) && dataRole.description) {
            // VALIDATE IF USER HAS SUPERADMIN ROLE
            let userRef = db.collection("users");
            userRef
              .where("email", "==", req.body.admin_user)
              .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
              .get()
              .then((snap) => {
                if (!snap.empty) {
                  // IF USER SENT IS SUPERADMIN ADD THE NEW  ROLE
                  let roleRef = db.collection("roles").doc(dataRole.role_id.toString());
                  let getDoc = roleRef
                    .get()
                    .then((doc) => {
                      if (!doc.exists) {
                        // CREATE THE NEW ROLE
                        roleRef.set(dataRole);
                        res.send({
                          code: 1,
                          message: "Role created successfully",
                        });
                      } else {
                        // ROLE ALREADY EXISTS
                        res.send({
                          code: 4,
                          message:
                            "Role already exists, try with another number",
                        });
                      }
                    })
                    .catch({});
                } else {
                  res.send({
                    code: 2,
                    message:
                      "Unauthorized admin_user, provide a valid superadministrator",
                  });
                }
              });
          } else {
            res.send({
              code: 3,
              message:
                "Incomplete or invalid parameters, please review the documentation first",
            });
          }
        } catch (error) {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF READ ROLE-----------------
router.post("/CF_read_role", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF ROLE ALREADY EXISTS
        let dataRole = req.body;
        if (dataRole.admin_user && Number.isInteger(dataRole.role_id)) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN RETURN ROLE
                let roleRef = db
                  .collection("roles")
                  .doc(dataRole.role_id.toString());
                let getDoc = roleRef.get().then((doc) => {
                  if (doc.exists) {
                    // RETURN ROLE
                    res.send({
                      code: 1,
                      message: "Data returned successfully",
                      data: doc.data(),
                    });
                  } else {
                    // ROLE DOES NOT EXISTS
                    res.send({
                      code: 4,
                      message: "Role does not exist, try with another name",
                    });
                  }
                });
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator",
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF UPDATE ROLE-----------------
router.put("/CF_update_role", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF ROLE ALREADY EXISTS
        let dataRole = req.body;
        if (dataRole.admin_user && Number.isInteger(dataRole.role_id)) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN UPDATE ROLE
                let roleRef = db
                  .collection("roles")
                  .doc(dataRole.role_id.toString());
                let getDoc = roleRef.get().then((doc) => {
                  if (doc.exists) {
                    // UPDATE ROLE
                    delete dataRole.admin_user;
                    delete dataRole.role_id;
                    roleRef.update(dataRole);
                    res.send({
                      code: 1,
                      message: "Role updated successfully"
                    });
                  } else {
                    // ROLE DOESN'T EXISTS
                    res.send({
                      code: 4,
                      message: "Role does not exist, try with another name"
                    });
                  }
                });
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator"
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first"
          });
        }
      }
    });
  }
});

//-----------------CF DELETE ROLE-----------------
router.delete("/CF_delete_role", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF ROLE ALREADY EXISTS
        let dataRole = req.body;
        if (dataRole.admin_user && Number.isInteger(dataRole.role_id)) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN DELETE ROLE
                let roleRef = db
                  .collection("roles")
                  .doc(dataRole.role_id.toString());
                let getDoc = roleRef
                  .get()
                  .then((doc) => {
                    if (doc.exists) {
                      // DELETE ROLE
                      roleRef.delete();
                      res.send({
                        code: 1,
                        message: "Role deleted successfully",
                      });
                    } else {
                      // ROLE DOESN'T EXISTS
                      res.send({
                        code: 4,
                        message:
                          "Role does not exist, try with another name",
                      });
                    }
                  })
                  .catch({});
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator",
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});
