const express = require('express')
const router = express.Router()
const moment = require('moment-timezone');
moment().format();
module.exports = router

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
const admin = require('firebase-admin')

let db = admin.firestore()

//-----------------CF GET ALL ROOMS FROM A COMPANY-----------------
router.get('/:company_id/CF_read_rooms', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        let company_id = req.params.company_id;
        let rooms = []
        db.collection('rooms').where("company_id", "==", company_id).where("active", "==", 1)
          .get()
          .then(snapshot => {
            snapshot.docs.forEach(snap => {
              let new_room = snap.data()
              delete new_room.active
              delete new_room.admin_user
              delete new_room.creation_date
              delete new_room.list_images
              delete new_room.prices
              delete new_room.services
              delete new_room.company_id
              rooms.push(new_room)

            })
            res.send({
              code: 1,
              message: 'Rooms returned successfully',
              data: rooms
            })
          })
      }
    })
  }
})

//-----------------CF CREATE NEW ROOM-----------------
router.post('/:company_id/CF_create_room', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let company_id = req.params.company_id
        let dataRoom = req.body
        if (
          dataRoom.admin_user &&
          dataRoom.name &&
          dataRoom.active &&
          dataRoom.prices &&
          dataRoom.icon_image &&
          dataRoom.description &&
          dataRoom.list_images
        ) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '>=', req.app.locals.global_vars.companyAdmin)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS ALLOWED THEN ADD THE NEW  ROOM
                dataRoom.room_id = dataRoom.name
                  .replace(/ /g, '_')
                  .toLowerCase()
                dataRoom.company_id = company_id
                dataRoom.creation_date = new Date()
                let roomRef = db
                  .collection('rooms')
                  .doc(company_id + "__" + dataRoom.room_id)
                let getDoc = roomRef
                  .get()
                  .then(doc => {
                    if (!doc.exists) {
                      // CREATE THE NEW ROOM
                      roomRef.set(dataRoom)
                      res.send({
                        code: 1,
                        message: 'Room created successfully',
                        room_id: dataRoom.room_id
                      })
                    } else {
                      // ROOM ALREADY EXISTS
                      res.send({
                        code: 4,
                        message: 'Room already exists, try with another name'
                      })
                    }
                  })
                  .catch({})
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid user'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF READ ROOM-----------------
router.post('/:company_id/CF_read_room', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF ROOM ALREADY EXISTS
        let dataRoom = req.body
        let company_id = req.params.company_id
        if (dataRoom.room_id) {
          // PROMISE THAT RETURNS A ROOM IF EXISTS  v .where('room_id', '==', "sala_verde") .where('company_id', '==', req.params.company_id)
          let roomRef = db.collection('rooms').doc(company_id + "__" + dataRoom.room_id)
          roomRef.get().then(doc => {
            if (doc.exists) {
              let room = doc.data()
              delete room.creation_date
              delete room.admin_user
              delete room.active
              // RETURN ROO MdataRoom.room_id
              res.send({
                code: 1,
                message: 'Data returned successfully',
                data: room
              })
            } else {
              // ROOM DOESN'T EXISTS
              res.send({
                code: 4,
                message: 'Room does not exist'
              })
            }
          })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF UPDATE ROOM-----------------
router.put('/:company_id/CF_update_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataRoom = req.body
        if (dataRoom.admin_user && dataRoom.company_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '==', req.app.locals.global_vars.superAdminRole)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN RETURN COMPANY
                let companyRef = db
                  .collection('companies')
                  .doc(dataRoom.company_id)
                let getDoc = companyRef.get().then(doc => {
                  if (doc.exists) {
                    // UPDATE COMPANY
                    delete dataRoom.admin_user
                    delete dataRoom.company_id
                    companyRef.update(dataRoom)
                    res.send({
                      code: 1,
                      message: 'Company updated successfully'
                    })
                  } else {
                    // COMPANY ALREADY EXISTS
                    res.send({
                      code: 4,
                      message: 'Company does not exist, try with another name'
                    })
                  }
                })
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid superadministrator'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF DELETE ROOM-----------------
router.delete('/:company_id/CF_delete_company', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // IF VALID THEN VALIDATE IF COMPANY ALREADY EXISTS
        let dataRoom = req.body
        if (dataRoom.admin_user && dataRoom.company_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection('users')
          userRef
            .where('email', '==', req.body.admin_user)
            .where('role_id', '==', req.app.locals.global_vars.superAdminRole)
            .get()
            .then(snap => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN DELETE COMPANY
                let companyRef = db
                  .collection('companies')
                  .doc(dataRoom.company_id)
                let getDoc = companyRef.get().then(doc => {
                  if (doc.exists) {
                    // DELETE COMPANY
                    companyRef.delete()
                    res.send({
                      code: 1,
                      message: 'Company deleted successfully'
                    })
                  } else {
                    // COMPANY ALREADY EXISTS
                    res.send({
                      code: 4,
                      message: 'Company does not exist, try with another name'
                    })
                  }
                })
              } else {
                res.send({
                  code: 2,
                  message:
                    'Unauthorized admin_user, provide a valid superadministrator'
                })
              }
            })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF GET AVAILABLE CALENDAR-----------------
router.post('/:company_id/:room_id/CF_calendar', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        // GET SCHEDULE
        let dataRoom = req.body
        let company_id = req.params.company_id
        let room_id = req.params.room_id
        let schedule = []
        let prices = {}
        let busy_time_range = []
        let special_prices = []
        let numberday
        dataRoom.company_id = company_id
        dataRoom.room_id = room_id
        dataRoom.available_calendar = []
        if (validateDateStructure(dataRoom.date)) {
          let numday = new Date(dataRoom.date)
          numday = numday.getUTCDay()
          // PROMISE THAT RETURNS A ROOM IF EXISTS
          let companyRef = db.collection('companies').doc(company_id)
          companyRef.get().then(doc => {
            if (doc.exists) {
              schedule = doc.get("schedule")
              // VALIDATE IF THE COMPANY WORKS IN THIS DATE
              if (schedule[numday].time_range !== "") {

                // VALIDATE IF HOLIDAY
                let holidays = []
                let dataRiffRef = db.collection("data_riff").doc("holidays")
                dataRiffRef.get().then(doc => {
                  holidays = doc.get(dataRoom.date.split("-")[0])
                  // GET INFO ABOUT ROOM
                  let roomRef = db.collection("rooms").doc(company_id + "__" + room_id)
                  roomRef.get().then(doc => {
                    if (doc.exists) {
                      prices = doc.get("prices")
                      dataRoom.base_price = prices.base_price
                      let times
                      if (!holidays.includes(dataRoom.date)) {
                        // IF NOT HOLIDAY
                        numberday = numday
                        times = schedule[numday].time_range
                      } else {
                        // IF HOLIDAY
                        numberday = 7
                        times = schedule[numberday].time_range
                      }
                      special_prices = prices.special_prices[numday];
                      dataRoom.active_times = times
                      times = times.split("-")

                      // GENERATING CALENDAR
                      for (var i = parseInt(times[0]); i < parseInt(times[1]); i++) {
                        let time_range = i.toString() + "-" + (i + 1).toString()
                        dataRoom.available_calendar.push({
                          "time_range": time_range,
                          "price": dataRoom.base_price,
                          "busy": false
                        })
                      }
                      let reservationRef = db.collection("reservations").where("company_id", "==", company_id).where("room_id", "==", room_id).where("date", "==", dataRoom.date)
                      // GET RESERVATIONS TO DISCARD UNAVAILABLE TIMES
                      reservationRef.get().then(snapshot => {
                        snapshot.docs.forEach(snap => {
                          busy_time_range.push(snap.get("time_range"))
                        })

                        // VALIDATE IF UNAVAILABLE TIME THEN  BUSY ==TRUE
                        busy_time_range.forEach(res => {
                          res.forEach(el => {
                            for (let i = 0; i < dataRoom.available_calendar.length; i++) {
                              if (el.time === dataRoom.available_calendar[i].time_range) {
                                dataRoom.available_calendar[i].busy = true
                                break;
                              }
                            }
                          });
                        });

                        // VALIDATE SPECIAL PRICES
                        for (let i = 0; i < special_prices.length; i++) {
                          for (let j = 0; j < dataRoom.available_calendar.length; j++) {
                            if (special_prices[i].time_range === dataRoom.available_calendar[j].time_range) {
                              dataRoom.available_calendar[j].price = special_prices[i].price
                              break
                            }
                          }
                        }

                        let currentDate = moment().tz('America/Bogota').format()//new Date().toLocaleString("pt-BR", { timeZone: "America/Bogota" })
                        let tmpdate = currentDate.split("T")[0]
                        // let curDate = currentHour.split(", ")[0].split("/")
                        // curDate = curDate[2] + "-" + curDate[0] + "-" + curDate[1]
                        if (tmpdate === dataRoom.date) {
                          // initial hour from Now
                          currentHour = parseInt(currentDate.split("T")[1].split(":")[0])
                          let counter = 0
                          while (counter < dataRoom.available_calendar.length) {
                            if (parseInt(dataRoom.available_calendar[counter].time_range.split("-")[0]) <= currentHour) {
                              dataRoom.available_calendar.splice(counter, 1)
                            } else {
                              counter++
                            }
                          }
                        }

                        res.send({
                          code: 1,
                          message: 'Data returned successfully',
                          data: dataRoom
                        })

                      })

                    } else {
                      res.send({
                        code: 4,
                        message: 'Company or room does not exists'
                      })
                    }
                  })

                })

              } else {
                // DIA NO LABORAL
                res.send({
                  code: 2,
                  message: 'Company does not work this day'
                })
              }

            } else {
              // COMPANY DOESN'T EXISTS
              res.send({
                code: 4,
                message: 'Company does not exist'
              })
            }
          })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

function validateDateStructure(date) {
  // let a = date.split("-")
  // let structureLength = [4, 2, 2]
  // let counter = 0
  // let res = false
  // for (let i = 0; i < a.length; i++) {
  //   if (a[i].length !== structureLength[i]) {
  //     res = false;
  //     break;
  //   } else {
  //     res = true;
  //   }

  // }
  // return res
  return true///moment(date, 'YYYY-MM-DD').isValid()
}

function isFutureDate(date) {
  if (moment().format("YYYY-MM-DD") <= moment(date).format("YYYY-MM-DD")) {
    return true
  } else {
    return false
  }
}