const express = require("express");
const router = express.Router();
module.exports = router;

const functions = require("firebase-functions");
var jwt = require("jsonwebtoken");
const admin = require("firebase-admin");

let db = admin.firestore();

//-----------------CF GET AVAILABLE SERVICES-----------------
router.get("/CF_read_available_services", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        let services = [];
        db.collection("services")
          .get()
          .then((snapshot) => {
            snapshot.docs.forEach((snap) => {
              services.push(snap.data());
            });
            res.send({
              code: 1,
              message: "Services returned successfully",
              data: services,
            });
          });
      }
    });
  }
});

//-----------------CF CREATE NEW SERVICE-----------------
router.post("/CF_create_service", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF SERVICE ALREADY EXISTS
        let dataService = req.body;
        if (
          dataService.admin_user &&
          dataService.name &&
          dataService.active &&
          dataService.icon_image
        ) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN ADD THE NEW  SERVICE
                dataService.service_id = dataService.name
                  .replace(/ /g, "_")
                  .toLowerCase();
                let serviceRef = db
                  .collection("services")
                  .doc(dataService.service_id);
                let getDoc = serviceRef
                  .get()
                  .then((doc) => {
                    if (!doc.exists) {
                      // CREATE THE NEW SERVICE
                      serviceRef.set(dataService);
                      res.send({
                        code: 1,
                        message: "Service created successfully",
                        service_id: dataService.service_id,
                      });
                    } else {
                      // SERVICE ALREADY EXISTS
                      res.send({
                        code: 4,
                        message:
                          "Service already exists, try with another name",
                      });
                    }
                  })
                  .catch({});
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator",
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF READ SERVICE-----------------
router.post("/CF_read_service", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF SERVICE ALREADY EXISTS
        let dataService = req.body;
        if (dataService.service_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let serviceRef = db
            .collection("services")
            .doc(dataService.service_id);
          let getDoc = serviceRef.get().then((doc) => {
            if (doc.exists) {
              // RETURN SERVICE
              res.send({
                code: 1,
                message: "Data returned successfully",
                data: doc.data(),
              });
            } else {
              // SERVICE DOES NOT EXISTS
              res.send({
                code: 4,
                message: "Service does not exist, try with another name",
              });
            }
          });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF UPDATE SERVICE-----------------
router.put("/CF_update_service", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF SERVICE ALREADY EXISTS
        let dataService = req.body;
        if (dataService.admin_user && dataService.service_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN UPDATE SERVICE
                let serviceRef = db
                  .collection("services")
                  .doc(dataService.service_id);
                let getDoc = serviceRef.get().then((doc) => {
                  if (doc.exists) {
                    // UPDATE SERVICE
                    delete dataService.admin_user;
                    delete dataService.service_id;
                    serviceRef.update(dataService);
                    res.send({
                      code: 1,
                      message: "Service updated successfully",
                    });
                  } else {
                    // SERVICE DOESN'T EXISTS
                    res.send({
                      code: 4,
                      message: "Service does not exist, try with another name",
                    });
                  }
                });
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator",
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});

//-----------------CF DELETE SERVICE-----------------
router.delete("/CF_delete_service", (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf("Bearer ") === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: "Missing Authorization Header" });
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(" ")[1];
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: "Invalid or expired token, please generate a new one",
        });
      } else {
        // IF VALID THEN VALIDATE IF SERVICE ALREADY EXISTS
        let dataService = req.body;
        if (dataService.admin_user && dataService.service_id) {
          // VALIDATE IF USER HAS SUPERADMIN ROLE
          let userRef = db.collection("users");
          userRef
            .where("email", "==", req.body.admin_user)
            .where("role_id", "==", req.app.locals.global_vars.superAdminRole)
            .get()
            .then((snap) => {
              if (!snap.empty) {
                // IF USER SENT IS SUPERADMIN DELETE SERVICE
                let serviceRef = db
                  .collection("services")
                  .doc(dataService.service_id);
                let getDoc = serviceRef
                  .get()
                  .then((doc) => {
                    if (doc.exists) {
                      // DELETE SERVICE
                      serviceRef.delete();
                      res.send({
                        code: 1,
                        message: "Service deleted successfully",
                      });
                    } else {
                      // SERVICE DESON'T EXISTS
                      res.send({
                        code: 4,
                        message:
                          "Service does not exist, try with another name",
                      });
                    }
                  })
                  .catch({});
              } else {
                res.send({
                  code: 2,
                  message:
                    "Unauthorized admin_user, provide a valid superadministrator",
                });
              }
            });
        } else {
          res.send({
            code: 3,
            message:
              "Incomplete or invalid parameters, please review the documentation first",
          });
        }
      }
    });
  }
});
