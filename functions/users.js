const express = require('express')
const router = express.Router()
module.exports = router

const functions = require('firebase-functions')
var jwt = require('jsonwebtoken')
const admin = require('firebase-admin')
global.atob = require('atob')
global.Blob = require('node-blob')
// const storage = admin.storage()

let db = admin.firestore()

// const storage = admin.storage()

//-----------------CF CREATE USER-----------------
router.post('/CF_create_user', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    const bearerToken = req.headers.authorization.split(' ')[1]
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate new one'
        })
      } else {
        let dataUser = req.body
        if (
          dataUser.email &&
          dataUser.active &&
          dataUser.role_id &&
          dataUser.full_name &&
          dataUser.band_id &&
          dataUser.company_id &&
          dataUser.identification_card &&
          dataUser.phone_number
        ) {
          // IF VALID VALIDATE IF USER EXISTS
          dataUser.creation_date = new Date()
          let userRef = db.collection('users').doc(dataUser.email)
          userRef.get().then(doc => {
            if (!doc.exists) {
              // CREATE THE NEW USER
              let arr_bands = []
              for (let i = 0; i < dataUser.band_id.length; i++) {
                arr_bands.push(
                  dataUser.band_id[i].toLowerCase().replace(' ', '_')
                )
              }
              dataUser.band_id = arr_bands
              dataUser.fav_companies = []
              dataUser.configurations = {
                notifications: {
                  sms: true,
                  push: true,
                  email: true
                }
              }
              delete dataUser.password
              delete dataUser.confirmPassword
              userRef.set(dataUser)
              res.send({
                code: 1,
                message: 'User created successfully'
              })
            } else {
              // USER ALREADY EXISTS
              res.send({
                code: 2,
                message: 'User already exists, try with another email'
              })
            }
          })
        } else {
          // INCOMPLETE PARAMS
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF READ USER-----------------
router.post('/CF_read_user', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate new one'
        })
      } else {
        // IF VALID RETURN USER
        if (req.body.email) {
          // IF EXISTS THE REQUEST EMAIL
          let userRef = db.collection('users').doc(req.body.email)
          userRef.get().then(doc => {
            if (doc.exists) {
              let bands = doc.data().band_id
              let bands_result = []
              if (bands.length > 0) {
                let temp_arr_bands = []
                db.collection('bands')
                  .get()
                  .then(snapshot => {
                    snapshot.docs.forEach(snap => {
                      temp_arr_bands.push(snap.data())
                    })
                    bands.forEach(el => {
                      temp_arr_bands.forEach(el2 => {
                        if (el == el2.band_id) {
                          bands_result.push(el2)
                        }
                      })
                    })

                    let user_data = doc.data()
                    user_data.band_id = bands_result
                    // RETURN THE USER
                    res.send({
                      code: 1,
                      message: 'Data returned successfully',
                      data: user_data
                    })
                  })
              } else {
                // RETURN THE USER
                res.send({
                  code: 1,
                  message: 'Data returned successfully',
                  data: doc.data()
                })
              }
            } else {
              // USER EXISTS
              res.send({
                code: 2,
                message: 'User not found'
              })
            }
          })
        } else {
          // REQUESTED USER UNDEFINED
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF UPDATE USER-----------------
router.put('/CF_update_user', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    const bearerToken = req.headers.authorization.split(' ')[1]
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired Token, please generate a new one'
        })
      } else {
        // IF VALID VALIDATE IF USER EXISTS
        if (req.body.email) {
          let userRef = db.collection('users').doc(req.body.email)
          userRef.get().then(doc => {
            if (doc.exists) {
              // UPDATE USER
              let dataUser = req.body
              delete dataUser.email
              userRef.update(dataUser)
              res.send({
                code: 1,
                message: 'User updated successfully'
              })
            } else {
              // USER DOESN'T EXISTS
              res.send({
                code: 2,
                message: 'User does not exist, try with another email'
              })
            }
          })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF DELETE USER-----------------
router.delete('/CF_delete_user', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    const bearerToken = req.headers.authorization.split(' ')[1]
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate a new one'
        })
      } else {
        if (req.body.email) {
          // IF VALID VALIDATE IF USER EXISTS
          let userRef = db.collection('users').doc(req.body.email)
          userRef.get().then(doc => {
            if (doc.exists) {
              // DELETE USER
              let dataUser = userRef.delete()

              res.send({
                code: 1,
                message: 'User deleted successfully'
              })
            } else {
              // USER DOESN'T EXISTS
              res.send({
                code: 2,
                message: 'User does not exist, try with another email'
              })
            }
          })
        } else {
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})

//-----------------CF UPDATE CONFIGS-----------------
router.put('/CF_update_config', (req, res) => {
  if (
    !req.headers.authorization ||
    req.headers.authorization.indexOf('Bearer ') === -1
  ) {
    return res
      .status(401)
      .json({ code: 401, message: 'Missing Authorization Header' })
  } else {
    // verify auth credentials
    // VALIDATE IF THE TOKEN IS NOT EXPIRED
    const bearerToken = req.headers.authorization.split(' ')[1]
    jwt.verify(bearerToken, req.app.locals.global_vars.APIPassword, (
      err,
      decoded
    ) => {
      if (err) {
        res.status(401).send({
          code: 401,
          message: 'Invalid or expired token, please generate new one'
        })
      } else {
        let req_data = req.body
        if (
          req_data.email,
          req_data.configurations
        ) {
          // IF VALID VALIDATE IF USER EXISTS
          let userRef = db.collection('users').doc(req_data.email)
          userRef.get().then(doc => {
            if (doc.exists) {
              userRef.set({
                configurations: req_data.configurations
              }, { merge: true })
              res.send({
                code: 1,
                message: 'Configurations updated successfully'
              })
            } else {
              // USER ALREADY EXISTS
              res.send({
                code: 2,
                message: 'User does not exist, try with another email'
              })
            }
          })
        } else {
          // INCOMPLETE PARAMS
          res.send({
            code: 3,
            message:
              'Incomplete or invalid parameters, please review the documentation first'
          })
        }
      }
    })
  }
})